# Weather Forecast App

A peek into the old React: a class-based Weather forecast app

- Fetching data from https://geocoding-api.open-meteo.com and https://api.open-meteo.com

- The code is based on https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350912#overview

- fetch weather from web API, weather data saved in localstorage

- **[NEW]** See my version of the app, written in modern ReactJS : https://gitlab.com/java-remote-ee27-course/reactjs/weatherforecastapp-new


## Pictures

![weather1](./public/weather1.png)


## Modified by

Katlin

## Acknowledgement

- Styles based on J. Schmedtmann's https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350912
- I have modified the initial code by following J. Schmedtmann's course https://www.udemy.com/course/the-ultimate-react-course/learn/lecture/37350912 (initial code was written by J. Schmedtmann, my version is a modification of that).
